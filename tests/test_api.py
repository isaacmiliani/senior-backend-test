# encoding: utf-8
"""API Testing."""
import logging
import json
import tornado.escape
import tornado.testing
import tornado.ioloop
import pytest
import sys
sys.path.insert(0, 'api/')
from model import User
import api as api_module
from config import Options
Options(environment='test')


class TestApp(tornado.testing.AsyncHTTPTestCase):
    def setUp(self):
        user = User(email="admin@test.com",
                    password="password")
        logging.debug("setting up the user for testing: %s", user.password)
        tornado.ioloop.IOLoop.current().run_sync(user.insert)
        super(TestApp, self).setUp()

    def tearDown(self):
        exist = True
        while exist:
            user = User(email="admin@test.com",
                        password='')
            exist = tornado.ioloop.IOLoop.current().run_sync(user.find)
            logging.debug("user exist %s", exist)
            if exist:
                tornado.ioloop.IOLoop.current().run_sync(user.delete)

    def get_app(self):
        return api_module.make_app()

    def test_options(self):
        response = self.fetch('/debug',
                              method="POST",
                              headers={},
                              body=json.dumps({"email": "admin@test.com",
                                               "password": "password"}))
        self.assertEqual(response.code, 200)
        response_body = tornado.escape.json_decode(response.body)
        logging.debug(response_body)
        self.assertEqual({"log_file_max_size": 100000000,
                          "log_rotate_mode": "size",
                          "env": "test",
                          "log_file_num_backups": 10,
                          "logging": "info",
                          "log_file_prefix": None,
                          "log_rotate_interval": 1,
                          "log_rotate_when": "midnight",
                          "log_to_stderr": None,
                          "help": None},
                         response_body)

    def test_options_auth(self):
        response = self.fetch('/debug',
                              method="POST",
                              headers={},
                              body=json.dumps({"email": "admin@test.com",
                                               "password": "245"}))
        self.assertEqual(response.code, 401)

        response = self.fetch('/debug',
                              method="POST",
                              headers={},
                              body=json.dumps({"email": "admin@test.com",
                                               "password": "password"}))
        self.assertEqual(response.code, 200)

    def test_status(self):
        response = self.fetch('/status',
                              method="GET")
        self.assertEqual(response.code, 200)
        self.assertEqual(response.body.decode('utf-8'), 'ok')


def main():
    """Main."""
    logging.basicConfig()
    logging.getLogger().setLevel(logging.DEBUG)
    logging.debug("Main.")


if __name__ == '__main__':
    tornado.testing.main()