# encoding: utf-8
"""Example."""
import asyncio
import pytest
import logging
import json
import sys
sys.path.insert(0, 'api/')
from model import User, PasswordEncrypt


class TestUser(object):
    @pytest.mark.asyncio
    async def test_user(self):
        logging.info(self)
        res = User(email='test@wide-eyes.it',
                   password='password')
        print(res)
        print(res.email)
        user = res
        uid = await user.insert()
        logging.info(uid)
        user.password = '134256'
        await user.update()
        print(user.db_id)
        user.email = 'test@wide-eyes.it'
        result = await user.find()
        print("user.password: {}".format(user.password))
        print(user.db_id)
        print(result)

        user_diff_password = User(email=user.email,
                                  password='1623')
        assert user.password != user_diff_password.password

    @pytest.mark.asyncio
    async def test_auth(self):
        user = User(email="notest@foo.com",
                    password="744")
        user_diff_password = User(email='test@test.com',
                                  password='1623')
        assert user.password != user_diff_password.password

        assert user.check_password(password='744')

    def test_encrypt(self):
        assert PasswordEncrypt.encrypt(raw_password='1623') != PasswordEncrypt.encrypt(raw_password='744')
        salted_password = PasswordEncrypt.encrypt(raw_password='1623')
        assert PasswordEncrypt.check_password(hashed_password=salted_password,
                                              raw_password='1623')


def main():
    """Main."""
    logging.basicConfig()
    logging.getLogger().setLevel(logging.DEBUG)
    logging.debug("Main.")


if __name__ == '__main__':
    main()
