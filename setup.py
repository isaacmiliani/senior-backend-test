from setuptools import setup, find_packages

setup(
    name = 'api',
    version = '1.0.0',
    url = '',
    author = 'Wide Eyes Technologies',
    author_email = '',
    description = 'Example API',
    packages = find_packages(),
    install_requires = [],
)