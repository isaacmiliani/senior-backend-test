# API Template

To serve as an example on how to create new APIs.


Prerequisites:

 - Install Python 3.6
 - Install a local MongoDB.
 - Create a users collection in db local.
 - Install the project dependencies in requirements.txt.